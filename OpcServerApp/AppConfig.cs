﻿using System.Configuration;
using System.IO;
using Opc.Ua;

namespace OpcServerApp
{
    public class AppConfig
    {
        private const string LogsConfigKey = "LogsConfigPath";
        private const string OpcItemsKey = "OpcItemsPath";
        private const string OpcWindowsConfigKey = "OpcWindowsConfig";
        private const string OpcLinuxConfigKey = "OpcLinuxConfig";

        public string OpcItemsPath { get; }

        public string LoggerConfigPath { get; }

        public string ServerConfigPath { get; }

        public bool IsMonoConfig { get; }

        public static AppConfig ParseConfig()
        {
            var appSettings = ConfigurationManager.AppSettings;

            // Logger
            var loggerConfigPath = appSettings[LogsConfigKey];
            if (string.IsNullOrEmpty(loggerConfigPath) || !File.Exists(loggerConfigPath))
            {
                throw new ConfigurationErrorsException(
                    $"Logger configuration file path is invalid or not set. Please check the \"{LogsConfigKey}\" param in the application settings."
                );
            }

            // OPC Items
            var opcItemsPath = appSettings[OpcItemsKey];
            if (string.IsNullOrEmpty(opcItemsPath) || !File.Exists(opcItemsPath))
            {
                throw new ConfigurationErrorsException(
                    $"OPC items file path is invalid or not set. Please check the \"{OpcItemsKey}\" param in the application settings."
                    );
            }

            // OPC Configuration
            var configKey = Utils.IsRunningOnMono() ? OpcLinuxConfigKey : OpcWindowsConfigKey;
            var serverConfigPath = appSettings[configKey];
            if (string.IsNullOrEmpty(serverConfigPath) || !File.Exists(serverConfigPath))
            {
                throw new ConfigurationErrorsException(
                    $"OPC Server configuration file path is invalid or not set. Please check the \"{configKey}\" param in the application settings.");
            }

            return new AppConfig(opcItemsPath, loggerConfigPath, serverConfigPath, Utils.IsRunningOnMono());
        }

        private AppConfig(string opcItemsPath, string loggerConfigPath, string serverConfigPath, bool isMonoConfig)
        {
            OpcItemsPath = opcItemsPath;
            LoggerConfigPath = loggerConfigPath;
            ServerConfigPath = serverConfigPath;
            IsMonoConfig = isMonoConfig;
        }
    }
}