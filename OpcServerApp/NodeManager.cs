﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Opc.Ua;
using Opc.Ua.Server;

namespace OpcServerApp
{
    public class NodeManager : CustomNodeManager2
    {
        public const string NamespaceUri = "opc-node-manager";

        private static readonly ILog _log = LogManager.GetLogger(typeof(NodeManager));

        private readonly OpcItem[] _opcItems;
        private readonly Dictionary<string, FolderState> _folders;

        public NodeManager(IServerInternal server, OpcItem[] opcItems)
            : base(server, NamespaceUri)
        {
            _opcItems = opcItems;
            _folders = new Dictionary<string, FolderState>();
        }

        public override void CreateAddressSpace(IDictionary<NodeId, IList<IReference>> externalReferences)
        {
            _log.Debug("Creating address space");

            lock (Lock)
            {
                if (!externalReferences.TryGetValue(ObjectIds.ObjectsFolder, out var references))
                {
                    externalReferences[ObjectIds.ObjectsFolder] = references = new List<IReference>();
                }

                foreach (var item in _opcItems)
                {
                    var parts = item.Path.Split('.');

                    // Create folders
                    FolderState parent = null;
                    for (int i = 0; i < parts.Length - 1; i++) // last part is a signal name
                    {
                        var name = string.Join(".", parts.Take(i + 1));

                        if (!_folders.ContainsKey(parts[i])) // if it's a new folder
                        {
                            var folderState = new FolderState(parent)
                            {
                                NodeId = new NodeId(name, NamespaceIndex),
                                BrowseName = new QualifiedName(name, NamespaceIndex),
                                DisplayName = name,
                                TypeDefinitionId = ObjectTypeIds.FolderType,
                                Description = string.Empty,
                                RolePermissions = new RolePermissionTypeCollection(),
                                UserRolePermissions = new RolePermissionTypeCollection()
                            };

                            AddReference(folderState, parent, references);
                            _folders.Add(name, folderState);
                        }

                        parent = _folders[name];
                    }

                    // Create a signal
                    var signalState = new DataItemState(parent)
                    {
                        NodeId = new NodeId(item.Path, NamespaceIndex),
                        BrowseName = new QualifiedName(parts.Last(), NamespaceIndex),
                        DisplayName = parts.Last(),
                        TypeDefinitionId = ObjectTypeIds.BaseObjectType,
                        Description = string.Empty,
                        RolePermissions = new RolePermissionTypeCollection(),
                        UserRolePermissions = new RolePermissionTypeCollection(),
                        AccessLevel = AccessLevels.CurrentReadOrWrite,
                        UserAccessLevel = AccessLevels.CurrentReadOrWrite,
                        DataType = item.DataType
                    };

                    if (item.Length == 1)
                    {
                        signalState.ValueRank = ValueRanks.Scalar;
                        signalState.Value = TypeConverter.GetDefaultValue((uint) item.DataType.Identifier);
                    }
                    else
                    {
                        signalState.ValueRank = ValueRanks.OneDimension;

                        var typeId = (uint) item.DataType.Identifier;
                        signalState.Value = Enumerable.Repeat(0, item.Length)
                            .Select(i => TypeConverter.GetDefaultValue(typeId))
                            .ToArray();
                    }

                    AddReference(signalState, parent, references);
                }

                // Add folders to the address space
                foreach (var folderState in _folders.Values)
                {
                    AddPredefinedNode(SystemContext, folderState);
                }
            }
        }

        public override void DeleteAddressSpace()
        {
            _log.Debug("Deleting address space");
            lock (Lock)
            {
                _folders.Clear();
            }
        }

        public override void Write(OperationContext context, IList<WriteValue> nodesToWrite,
            IList<ServiceResult> errors)
        {
            lock (Lock)
            {
                for (int i = 0; i < nodesToWrite.Count; i++)
                {
                    var node = FindPredefinedNode(nodesToWrite[i].NodeId, typeof(DataItemState));
                    if (node is DataItemState signalState)
                    {
                        var value = nodesToWrite[i].Value.Value;
                        if (value is Variant[] arr) 
                        {
                            // check that all array elements are of the same type as the signal
                            var signalType = (uint) signalState.DataType.Identifier;
                            if (arr.Any(a => (uint) a.TypeInfo.BuiltInType != signalType))
                            {
                                errors[i] = ServiceResult.Create(StatusCodes.BadTypeMismatch,
                                    "Signal type does not match the writing value type.");
                                continue;
                            }
                        }

                        // Update values
                        signalState.Value = nodesToWrite[i].Value.Value;
                        signalState.Timestamp = DateTime.Now;
                        signalState.ClearChangeMasks(SystemContext, false);
                        errors[i] = ServiceResult.Good;
                    }
                    else
                    {
                        errors[i] = ServiceResult.Create(StatusCodes.BadNodeIdUnknown,
                            "Could not find target node with id: {0}.", nodesToWrite[i].NodeId);
                    }
                }
            }
        }

        private void AddReference(BaseInstanceState item, FolderState parent, IList<IReference> references)
        {
            if (parent == null)
            {
                item.AddReference(ReferenceTypeIds.Organizes, true, ObjectIds.ObjectsFolder);
                references.Add(new NodeStateReference(ReferenceTypeIds.Organizes, false, item.NodeId));
            }
            else
            {
                parent.AddChild(item);
            }
        }
    }
}