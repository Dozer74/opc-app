﻿using System;
using System.IO;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using System.Threading;
using Opc.Ua.Configuration;

namespace OpcServerApp
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        private static AppConfig _config;

        static async Task Main(string[] args)
        {
            // Load config
            try
            {
                _config = AppConfig.ParseConfig();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred while reading the application configuration:\n{0}", e.Message);
                return;
            }

            // Configure logger
            XmlConfigurator.Configure(new FileInfo(_config.LoggerConfigPath));
            _log.InfoFormat("Using {0} logger config file", _config.LoggerConfigPath);

            // Read OPC items
            var opcItems = CsvReader.ReadItems(_config.OpcItemsPath);

            // Create app instance
            _log.InfoFormat("Using {0} server config file", _config.ServerConfigPath);
            var appInstance = new ApplicationInstance();
            await appInstance.LoadApplicationConfiguration(_config.ServerConfigPath, false);

            // Check app certificate
            var haveAppCertificate = await appInstance.CheckApplicationInstanceCertificate(false, 0);
            if (!haveAppCertificate)
            {
                var message = "The application instance certificate is invalid! Unable to start an OPC Server.";
                _log.Fatal(message);

                Console.WriteLine(message);
                Console.ReadLine();

                return;
            }

            // Create and start the server
            var server = new Server(opcItems);
            try
            {
                await appInstance.Start(server);
            }
            catch (Exception ex)
            {
                _log.FatalFormat("Error starting OPC server. {0}", ex);
                Console.WriteLine("Error starting OPC server. Check logs for more details.");
                return;
            }
            
            _log.Info("OPC Server started");
            Console.WriteLine("The OPC Server is running. Endpoint: " + server.EndpointUrl);
            Console.WriteLine("Press Ctrl+C to stop it.");

            // Wait till shutdown signal
            var waitHandle = new AutoResetEvent(false);
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                eventArgs.Cancel = true;
                waitHandle.Set();
            };
            waitHandle.WaitOne();

            Console.WriteLine("Server is shutting down...");
            server.Stop();
            appInstance.Stop();
        }
    }
}
