﻿using log4net;
using Opc.Ua;
using Opc.Ua.Server;

namespace OpcServerApp
{
    class Server : StandardServer
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Server));

        private readonly OpcItem[] _opcItems;
        
        public string EndpointUrl => GetEndpoints()[0].EndpointUrl;

        public Server(OpcItem[] opcItems)
        {
            _opcItems = opcItems;
        }

        protected override MasterNodeManager CreateMasterNodeManager(IServerInternal server, ApplicationConfiguration configuration)
        {
            _log.Debug("OPC Server creates master node manager");

            INodeManager[] nodeManagers = {
                new NodeManager(server, _opcItems), 
            };

            return new MasterNodeManager(server, configuration, null, nodeManagers);
        }

    }
}
