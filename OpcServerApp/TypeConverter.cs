﻿using System;
using Opc.Ua;

namespace OpcServerApp
{
    public static class TypeConverter
    {
        public static NodeId TypeToNodeId(string type)
        {
            switch (type.ToUpperInvariant())
            {
                case "SINGLE":
                case "REAL":
                    return DataTypes.Float;
                case "DOUBLE":
                    return DataTypes.Double;
                case "BOOL":
                    return DataTypes.Boolean;
                case "INT":
                case "INT16":
                    return DataTypes.Int16;
                case "DINT":
                case "INT32":
                    return DataTypes.Int32;
                case "LONG":
                case "INT64":
                    return DataTypes.Int64;
                case "CHAR":
                case "STRING":
                    return DataTypes.String;
                default:
                    throw new ArgumentException($"Unknown type: {type}");
            }
        }

        public static Type NodeIdToType(uint type)
        {
            switch (type)
            {
                case DataTypes.Float: return typeof(Single);
                case DataTypes.Double: return typeof(Double);
                case DataTypes.Boolean: return typeof(Boolean);
                case DataTypes.Int16: return typeof(Int16);
                case DataTypes.Int32: return typeof(Int32);
                case DataTypes.Int64: return typeof(Int64);
                case DataTypes.String: return typeof(String);
                default:
                    throw new ArgumentException($"Unknown type: {type}");
            }
        }

        public static object GetDefaultValue(uint type)
        {
            var t = NodeIdToType(type);
            if (t == typeof(string)) // returns empty string instead of null
            {
                return string.Empty;
            }
            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }
    }
}
