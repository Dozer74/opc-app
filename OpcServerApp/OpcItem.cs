﻿using Opc.Ua;

namespace OpcServerApp
{
    public class OpcItem
    {
        public string Path { get; }
        public NodeId DataType { get; }
        public int Length { get; }

        public OpcItem(string path, NodeId dataType, int length = 1)
        {
            Path = path;
            DataType = dataType;
            Length = length;
        }
    }
}