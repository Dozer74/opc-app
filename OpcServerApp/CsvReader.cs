﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;

namespace OpcServerApp
{
    public static class CsvReader
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Server));

        public static OpcItem[] ReadItems(string path)
        {
            var result = new List<OpcItem>();

            _log.InfoFormat("Reading OPC items from {0}", path);
            var lines = File.ReadLines(path)
                .Where(l => !string.IsNullOrEmpty(l));

            foreach (var line in lines)
            {
                var parts = line.Split(';', '\t');
                try
                {
                    var name = parts[0];
                    var type = TypeConverter.TypeToNodeId(parts[1]);
                    var length = parts.Length == 3
                        ? int.Parse(parts[2])
                        : 1;

                    result.Add(new OpcItem(name, type, length));
                }
                catch (Exception)
                {
                    _log.WarnFormat("Bad line: {0}", line);
                }
            }

            _log.InfoFormat("{0} items were read", result.Count);
            return result.ToArray();
        }
    }
}